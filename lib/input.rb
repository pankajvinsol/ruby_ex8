class Input
  def self.get_integer(message = "", error_message = "")
    begin
      print message    
      Integer(gets)
    rescue ArgumentError
      print error_message
      retry
    end
  end
end


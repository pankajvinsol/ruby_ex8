# Extend Array class to include a power function
# power function takes one number and increase the power of each array element to that no
# example:
# [1,2,3,4].power(2)
# => [1,4,9,16]

class Array
  def power(exponent)
    map { |value| value ** exponent }
  end
end


require_relative "../lib/array.rb"
require_relative "../lib/input.rb"

ERROR_MESSAGE = "Invalid Value, Try Again\n"

exponent = Input.get_integer("Enter Exponent : ", ERROR_MESSAGE)
size = Input.get_integer("Enter size of Array : ", ERROR_MESSAGE)
array = []

size.times do |index|
  array.push(Input.get_integer("Enter value #{ index + 1 } : ", ERROR_MESSAGE))
end

puts "Output:"
p array.power(exponent)

